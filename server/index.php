<?php
header('Content-Type: text/plain');

require dirname(__FILE__).'/DroidSmash.class.php';

$DroidSmash = new DroidSmash();

if(isset($_GET['get_scores'])){
    $DroidSmash->getScores();
} else {
    $DroidSmash->submitScore();
}