<?php

/**
 * DroidSmash High Score server
 *
 * @author Alan Pich
 * @date November 2012
 */
class DroidSmash {
    
    private $db_host = 'localhost';
    private $db_name = 'alan_droidsmash';
    private $db_user = 'manager';
    private $db_pass = 'shoreditchdev';
    private $db_table = 'scores';
    
    /**
     * Main Runtime
     */
    public function __construct(){
        $this->db = new mysqli($this->db_host,$this->db_user,$this->db_pass,$this->db_name);
        if($this->db->connect_errno){ die('FAIL'); };   
    }
    
    /**
     * Submit a score to the DB
     */
    public function submitScore(){
        $params = $_REQUEST;
        
        
        // Check for correct params
        if(!isset($params['score']) || !isset($params['name'])){
            die('INVALID');
        };
        // Dont allow extra params
        if(count($params) != 2 && count($params) != 3){
            die('INVALID');
        }
        
        
        // Sanitize
        $score = (int) $params['score'];
        $name = $this->sanitize($params['name']);
        $email = $this->sanitize($params['email']);
        $ip = $_SERVER['REMOTE_ADDR'];
        $time = time();
        
        // Add to database
        $sql = "INSERT INTO `".$this->db_table."` (`ip`,`time`,`score`,`name`,`email`) VALUES ('$ip',$time,$score,'$name','$email')";
        $this->db->query($sql);
        die('SUCCESS');
    }//
    
    
    /**
     * Get the 5 top scores
     */
    public function getScores(){
        $sql = "SELECT name,score FROM `".$this->db_table."` ORDER BY `score` DESC LIMIT 5";
        $result = $this->db->query($sql) or die('FAIL');
        $data = array();
        while($row = $result->fetch_assoc()) {
            $obj = new stdClass;
            $obj->name = $row['name'];
            $obj->score = (int) $row['score'];
            $data[] = $obj;
        }	
        die(json_encode($data));
    }//
    
    
    /**
     * Sanitize a sting for DB storage
     * @param string $str
     * @return sanitized string
     */
    private function sanitize($str){
       return mysql_real_escape_string($str); 
    }//
    
};// end class DroidSmash
