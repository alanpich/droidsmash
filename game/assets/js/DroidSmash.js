$(document).ready(function(){ DroidSmash.init(); });

TOUCH = ('ontouchstart' in window)? 'touchstart' : 'click';

DroidSmash = {

	id: '#frame'
	,ChanceOfAppleDroid: 1/4
	,MaxMisses: 5
	,holes: Array()
	,time: 1000
	,basetime: 1000
        ,submitUrl: 'http://dev.alanpich.com/steelmedia/droidsmash/server/'
	
	
	,isPlaying: true

	,init: function(){		
			this.frame = $(this.id);
			this.prepareHoles();
            this.StartScreen.init();
            this.OfflineGameOverScreen.init();
            this.OnlineGameOverScreen.init();
            this.LivesLeftDisplay.init();	

			
			$(document).trigger('DroidSmash-Init');
			
		//	this.showStartScreen();
		}//
		
	,startGame: function(){
			this.time = this.basetime;
                        
			this.Scoreboard.score = 0;
			this.Scoreboard.misses = 0;
                        this.Scoreboard.display();
                        
			this.StartScreen.hide();
                        this.OfflineGameOverScreen.hide();
                        this.OnlineGameOverScreen.hide();
                        
                        this.LivesLeftDisplay.reset();
                        this.LivesLeftDisplay.show();
			this.isPlaying = true;
                        setTimeout(function(){
                                DroidSmash.startLoop();
                            },1000);
		}//
		
	,prepareHoles: function(){
			var holes = this.frame.find('.hole');
			for(var k=0;k<holes.length;k++){
				this.holes.push( new DroidSmash_Hole(holes[k]) );
			};
		}//
		
		
	// Game ends
	,GameOver: function(){
			if(!this.isPlaying){return;};
			this.isPlaying = false;
			clearTimeout(this.timeout);
            // Reset all holes
            for(var k=0;k<this.holes.length;k++){
                this.holes[k].down();
            }                        
            // Which gameover screen to show?
            var online = true;
            if(online){
                DroidSmash.OnlineGameOverScreen.show();
            } else {
                DroidSmash.OfflineGameOverScreen.show();
            }
			$(document).trigger('DroidSmash-GameOver');
		}//
		
	// Show Start screen 
	,showStartScreen: function(){
			this.startScreen = $('#startScreen');
		}//
		
	// Start looping
	,startLoop: function(){
			this.loop();
		}//
				
	// Loop Event
	,loop: function(){
			// Pick a random hole (that isn't already active)
			var randomHole = Math.round(Math.random() * (this.holes.length-1));
			while( this.holes[randomHole].active ){
				var randomHole = Math.round(Math.random() * (this.holes.length-1));
			};
			
			// Activate a random hole
			this.holes[randomHole].activate();
			
			// Randomize next loop time
			var baseTime = this.time - this.Scoreboard.score;
			var rand = Math.round(Math.random()*baseTime);
			var time = this.getRandomTime();
			clearTimeout(this.timeout);
			this.timeout = setTimeout('DroidSmash.loop()',time);			
		}//
		
	// Get a random time +-50% of this.time
	,getRandomTime: function(){
			var baseTime = this.time;// - (20 * Math.round(this.Scoreboard.score/10))
			var random = Math.round((Math.random()*baseTime) + (baseTime/2));
			return random;
		}//
                
};// end DroidSmash


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

DroidSmash.Scoreboard = {
	
	score: 0
	,misses: 0
	,counter:0
	
	,inc: function(){
			this.score++;
			this.counter++;
			if(this.counter == 10){
				DroidSmash.time = DroidSmash.time * 0.85;
				this.counter = 0;
			};
			this.display();
		}//
	
	,dec: function(){
			this.score--;
			if(this.score<0){
				DroidSmash.GameOver();
			};
			this.display();
		}//	
		
	,miss: function(amt){
			if(amt==null){amt=1};
			this.misses+= amt;
                        DroidSmash.LivesLeftDisplay.update();
			if(this.misses >= DroidSmash.MaxMisses){
				DroidSmash.GameOver();
			};
		}//
		
	,display: function(){
			// Calculate units
			var score = this.getNumbers();
			

			$('#thou')[0].className = '_'+score.thou;
			$('#hund')[0].className = '_'+score.hund;
			$('#tens')[0].className = '_'+score.tens;
			$('#unit')[0].className = '_'+score.units;
			
		}//
		
	,getNumbers: function(){
			var str = this.score.toString()
			var arr = str.split('');
			var obj = {};
			
			if(str.length>0){
				obj.units = arr.pop()
			} else {obj.units = 0};
			if(str.length>1){
				obj.tens = arr.pop()				
			} else { obj.tens = 0;}
			if(str.length>2){
				obj.hund = arr.pop()				
			} else { obj.hund = 0;}
			if(str.length>3){
				obj.thou = arr.pop()				
			} else { obj.thou = 0;}
			
			return obj;
		}//
		
};// end DroidSmash.Scoreboard;


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

DroidSmash.LivesLeftDisplay = {
    
    elem: false
    
    ,init: function(){
            this.elem = $('#LivesLeftDisplay');
            this.number = this.elem.find('div').first();
            // How many lives do we start with?           
            this.hide();
        }//
        
    ,reset: function(){
        console.log(this);
        this.number[0].className = '_'+DroidSmash.MaxMisses;
        this.show();
    }
        
    ,hide: function(){ this.elem.hide(); }
    
    ,show: function(){ this.elem.show(); }
    
    ,update: function(){
        var misses = Math.floor(DroidSmash.Scoreboard.misses);
        var lives = this.elem.find('.life');
        var displayedLives = lives.length;
        var livesLeft = DroidSmash.MaxMisses - misses;
        this.elem.find('div')[0].className = '_'+livesLeft;

    }//
}


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

DroidSmash.StartScreen = {

	init: function(){
                this.elem = $('#StartScreen');
                this.elem.find('a').bind(TOUCH,function(){
                        DroidSmash.startGame();
                });
            }//
		
	,hide: function(){
                this.elem.hide();
            }//
	
	,show: function(){
                this.elem.show();
            }//	

};// end DroidSmash.StartScreen;


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

DroidSmash.OfflineGameOverScreen = {
    
    init: function(){
       // Grab DOM element
       this.elem = $('#OfflineGameOverScreen');
       // Bind restart button
       this.elem.find('a').bind('click',function(e){
           e.preventDefault();
           DroidSmash.startGame();
       });
       // Hide display
       this.hide();
    }//
    
    ,show: function(){
       this.elem.show();
    }//
    
    ,hide: function(){
        this.elem.hide();
    }//
    
};// end DroidSmash.OfflineGameOverScreen

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

DroidSmash.OnlineGameOverScreen = {
    
    init: function(){
		// Grab DOM element
		this.elem = $('#OnlineGameOverScreen');
		this.scoreList = this.elem.find('table');

		this.form = this.elem.find('form');
		this.form.submit(function(e){
			e.preventDefault();
			DroidSmash.OnlineGameOverScreen.submit();
		})
       
       	this.thankYouMessage = this.elem.find('.thankYouMessage').hide();
		
		this.startAgainButton = this.elem.find('.start-again').click(function(e){
				e.preventDefault();
				DroidSmash.startGame();
			});
            
		// Hide display
		this.hide();
    }//
    
    ,show: function(){
    	DroidSmash.SubmitScore.getScores();
    
       this.elem.show();
    }//
    
    ,hide: function(){
        this.elem.hide();
    }//
    
    
            
    ,submit: function(){
            var params = this.form.serialize()+'&score='+DroidSmash.Scoreboard.score;
            $.ajax({
                url: DroidSmash.submitUrl+'?'+params
                ,success: function(data){
                    DroidSmash.OnlineGameOverScreen.afterSubmit(data);
                }
            })
        }//
    
        
    ,afterSubmit: function(data){        
    		var H = DroidSmash.OnlineGameOverScreen.form.height();
            DroidSmash.OnlineGameOverScreen.form.hide();
            DroidSmash.OnlineGameOverScreen.thankYouMessage.css({height: H+"px"}).show();
        }//    
    
    
    
};// end DroidSmash.OnlineGameOverScreen


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


DroidSmash.SubmitScore = {
    
        
    getScores: function(){
        $.ajax({
            url: DroidSmash.submitUrl
            ,type: 'get'
            ,data: {
                get_scores: true
            }
            ,success: function(response){
                DroidSmash.SubmitScore.afterGetScores( eval(response) );
            }
        })
    }//
    
    ,afterGetScores: function(scores){
        // Add user's score to array
        scores.push({
            name: 'Your Score',
            score: DroidSmash.Scoreboard.score,
            isMine: true
        });
        
        scores.sort(function(a,b){
            return b.score - a.score;
        })
        
        DroidSmash.OnlineGameOverScreen.scoreList.html('');
        for(var k=0;k<scores.length;k++){
            var row = $('<tr><td>'+scores[k].name+'</td><td>'+scores[k].score+'</td></tr>');
            if(scores[k].isMine){ row.addClass('own')}
            DroidSmash.OnlineGameOverScreen.scoreList.append(row);
        }
    }//
    
}

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


DroidSmash_Hole = function(elem){

	this.elem = $(elem);
	this.elem.data('DS-hole',this);
	this.time = 1000;
	this.killTime = 200;
	this.id = this.elem.attr('id');
	this.active = false;
	this.type = false;
	
	this.init = function(){
			this.elem.bind(TOUCH,function(e){
				e.stopPropagation();
				e.preventDefault();
				$(this).data('DS-hole').onClick();
			});
		}//
	
	this.activate = function(){
			// Generate random number
			var rand = Math.random();
			// Remove kill class just incase it's still there
			this.elem.removeClass('dead');
			// If less than ratio, throw in a traitor
			if(rand < DroidSmash.ChanceOfAppleDroid){
				this.activate_apple();	
			} else {
				this.activate_droid();
			};
		}//
		
	this.activate_droid = function(){
			this.active = true;
			this.elem.addClass('droid');
			this.type = 'droid';
			var time = DroidSmash.getRandomTime();
			this.timeout = setTimeout('$("#'+this.id+'").data("DS-hole").miss()',time);
	
		}//
		
	this.activate_apple = function(){
			this.active = true;
			this.elem.addClass('apple');
			this.type = 'apple';
			var time = DroidSmash.getRandomTime();
			this.timeout = setTimeout('$("#'+this.id+'").data("DS-hole").down()',time);	
		}//
		
	this.miss = function(){
			DroidSmash.Scoreboard.miss(0.5);
			this.down();
		}//
		
	this.down = function(){
			this.active = false;
			this.elem.removeClass('droid').removeClass('apple');
			this.type = false;
		}//
		
	
	this.onClick = function(){
			if(!this.active){return;};
			this.elem.addClass('dead');
			clearTimeout(this.timeout);
			if(this.type == 'apple'){
				DroidSmash.Scoreboard.dec();
				DroidSmash.Scoreboard.miss();
				$(document).trigger('DroidSmash-Wrong');
			} else {
				DroidSmash.Scoreboard.inc();
				$(document).trigger('DroidSmash-Hit');
			};
			this.timeout = setTimeout('$("#'+this.id+'").data("DS-hole").down()',this.killTime);	
		}//
	
	
	this.init();
};// end DroidSmash_Hole
