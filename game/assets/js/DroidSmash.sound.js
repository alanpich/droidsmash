if(DroidSmash){

// Bind to startup/init event
$(document).bind('DroidSmash-Init',function(){	DroidSmash.Sounds.init(); });

$(document).bind('DroidSmash-Hit',function(){ DroidSmash.Sounds.play('crunch'); });
$(document).bind('DroidSmash-Wrong',function(){ DroidSmash.Sounds.play('wrong'); });
$(document).bind('DroidSmash-GameOver',function(){ DroidSmash.Sounds.play('gameover'); });

// DroidSmash.Sounds object //////////////////////////////////////////////////
DroidSmash.Sounds = {

	playerCount: 4
	
	,players: Array()
	,currentPlayer: Array()
	,crunchSrc: 'assets/sound/crunch.wav'
	,wrongSrc: 'assets/sound/wrong.wav'
	,gameoverSrc: 'assets/sound/game-over.wav'

	// Setup audioplayers ready
	,init: function(){
	
			// Create several crunch audio elements
			this.players['crunch'] = Array();
			this.currentPlayer['crunch'] = 0;
			for(var k=0; k<this.playerCount;k++){
				var audio = document.createElement('audio');
				audio.setAttribute('src',this.crunchSrc);
				audio.load();
				$('body').append(audio);
				this.players['crunch'].push(audio);
			};

			// Create several wrong audio elements
			this.players['wrong'] = Array();
			this.currentPlayer['wrong'] = 0;
			for(var k=0; k<this.playerCount;k++){
				var audio = document.createElement('audio');
				audio.setAttribute('src',this.wrongSrc);
				audio.load();
				$('body').append(audio);
				this.players['wrong'].push(audio);
			};
			
			// Create the game over sound
			var audio = document.createElement('audio');
			audio.setAttribute('src',this.gameoverSrc);
			audio.load();
			$('body').append(audio);
			this.players['gameover'] = audio;

		}//



	// Play a sound
	,play: function( sound ){
			switch(sound){
				case 'crunch':	this.play_crunch();		break;
				case 'wrong':	this.play_wrong();	 	break;
				case 'gameover':this.play_gameover();	break;
			};
		}//
		
		
	// Play a crunch sound
	,play_crunch: function(){
			
			var sound = this.players['crunch'][this.currentPlayer['crunch']];
			sound.currentTime = 0;
			sound.play();			
			
			// Move pointer to next audio file
			var next = this.currentPlayer['crunch'] + 1;
			if(next == this.players['crunch'].length){
				next = 0;
			};
			this.currentPlayer['crunch'] = next;
			
		}//
		
		
	// Play a crunch sound
	,play_wrong: function(){
			
			var sound = this.players['wrong'][this.currentPlayer['wrong']];
			sound.currentTime = 0;
			sound.play();			
			
			// Move pointer to next audio file
			var next = this.currentPlayer['wrong'] + 1;
			if(next == this.players['wrong'].length){
				next = 0;
			};
			this.currentPlayer['wrong'] = next;
			
		}//
		
		
	// Play game over sound
	,play_gameover: function(){
			var sound = this.players['gameover'];
			sound.currentTime = 0;
			sound.play();
		}//
	

};// end DroidSmash.Sounds
//////////////////////////////////////////////////////////////////////////////
};// end If Droidsmash
